class Player:
  def __init__(self):
    self.heart = 3
    self.score = 0
    self.miss = 0
    self.total = 0
    self.totalHeartsCollect = 0

  def subHeart(self):
    self.heart -= 1
  
  def plusHeart(self):
    self.heart += 1
    self.totalHeartsCollect += 1

  def is_game_over(self):
    return self.heart < 0

  def setHeart(self, heart):
    self.heart = heart

  def increase_total(self):
    self.total += 1
    self.miss = self.total - self.score - self.totalHeartsCollect
  
  def increase_score(self):
    self.score += 1
