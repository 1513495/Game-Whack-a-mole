import pygame
from const import *

class Mouse(pygame.sprite.Sprite):
  def __init__(self, position):
    super(Mouse, self).__init__()
    self.index = 0
    self.images = (
      pygame.image.load("assets/images/mouse/1.png"),
      pygame.image.load("assets/images/mouse/2.png"),
      pygame.image.load("assets/images/mouse/3.png"),
      pygame.image.load("assets/images/mouse/4.png"),
      pygame.image.load("assets/images/mouse/die.png"))

    self.image = self.images[self.index]
    self.position = [position[0], position[1]]
    self.rect = pygame.Rect(position[0], position[1], SIZE_ITEM, SIZE_ITEM)
    self.die = False
    self.timeDie = 10
    
  def update(self):
    if self.die:
      self.image = self.images[len(self.images) - 1]
      self.timeDie -= 1
      if self.timeDie < 0:
          self.kill()
    else:
      self.index += 1
      if self.index >= len(self.images) - 1:
          self.index = 0
      self.image = self.images[self.index]
      self.position[1] += 2
      self.rect =pygame.Rect(self.position[0], self.position[1], SIZE_ITEM, SIZE_ITEM)
    
  def attack(self):
    if self.position[1] > GAME_HEIGHT:
      return True
    return False

  def collision(self,hammer):
      return self.rect.colliderect(hammer)

  def to_die(self):
    self.die = True
