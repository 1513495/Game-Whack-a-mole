import pygame
from random import randint
from const import *

class Heart(pygame.sprite.Sprite):
  def __init__(self, position):
    super(Heart, self).__init__()
    self.index= 0
    self.images = (
      pygame.image.load("assets/images/heart/heart1.png"),
      pygame.image.load("assets/images/heart/heart2.png"),
      pygame.image.load("assets/images/heart/heart3.png"))
    
    self.image = self.images[self.index]
    self.position = [position[0], position[1]]
    self.rect = pygame.Rect(position[0], position[1], SIZE_ITEM, SIZE_ITEM)

  def update(self):
    if self.position[1] > GAME_HEIGHT:
      self.kill()
    else:
      self.index += 1
      if self.index >= len(self.images) - 1:
        self.index = 0
      self.image = self.images[self.index]
      self.position[1] += 2
      self.rect =pygame.Rect(self.position[0], self.position[1], SIZE_ITEM, SIZE_ITEM)

  def expire(self):
    return self.position[1] > GAME_WIDTH

  def collision(self, hammer):
    return self.rect.colliderect(hammer)
