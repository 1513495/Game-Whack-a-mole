import os
import pygame
import random

pygame.init()

PATH = "assets/sounds/"

HITS = PATH + "hits/"
MOUSES = PATH + "mouses/"
BOOMS = PATH + "booms/"
MUSIC_BACKGROUND = PATH + "music_background/"
HEART = PATH + 'heart/'
class Sounds:
	def __init__(self):
		self.hits = []
		self.booms = []
		self.mouses = []
		self.heart  = None
		self.music_background = None
		for dirs in os.listdir(PATH):
			for filesound in os.listdir(PATH + dirs):
				if filesound.endswith(".wav"):
					if(dirs == "booms"):
						self.booms.append(BOOMS + filesound)
					if(dirs == "mouses"):
						self.mouses.append(MOUSES + filesound)
					if(dirs == "hits"):
						self.hits.append(HITS + filesound)
					if(dirs == "music_background"): 
						self.music_background = MUSIC_BACKGROUND + filesound
					if(dirs == "heart"): 
						self.heart = HEART + filesound
	def hit(self):
		length = len(self.hits)
		idx = random.randrange(0, length)
		sound = pygame.mixer.Sound(self.hits[idx])
		sound.set_volume(0.3)
		sound.play()

	def mouse_die(self):
		length = len(self.mouses)
		idx = random.randrange(0, length)
		sound = pygame.mixer.Sound(self.mouses[idx])
		sound.set_volume(1.0)
		sound.play()

	def explosive(self):
		length = len(self.booms)
		idx = random.randrange(0, length)
		sound = pygame.mixer.Sound(self.booms[idx])
		sound.set_volume(1.0)
		sound.play()
	def take_heart(self):
		sound = pygame.mixer.Sound(self.heart)
		sound.set_volume(1.0)
		sound.play()
	def music(self):
		pygame.mixer.music.load(self.music_background)
		pygame.mixer.music.play(-1)
		pygame.mixer.music.set_volume(0.6)
