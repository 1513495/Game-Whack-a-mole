import pygame
import colors
from const import *

pygame.init()

class Background(pygame.Surface):
  def __init__(self):
    super(Background, self).__init__((WINDOW_WIDTH, WINDOW_HEIGHT))
    self.fill(colors.Black)
    pygame.draw.rect(self, colors.Gainsboro, (0, 0, GAME_WIDTH, GAME_HEIGHT))

    # Draw lines
    for i in range(LINES - 1):
      # left
      pygame.draw.line(self, colors.DarkGray, (i * WIDTH_LINE, 0), (i * WIDTH_LINE, GAME_HEIGHT), BORDER_WIDTH)
      # right
      pygame.draw.line(self, colors.DarkGray, ((i + 1) * WIDTH_LINE, 0), (( i + 1 ) * WIDTH_LINE, GAME_HEIGHT), BORDER_WIDTH)
    