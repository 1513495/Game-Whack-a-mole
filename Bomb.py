import pygame
import os
from const import *

PATH= "assets/images/bomb/"

class Bomb(pygame.sprite.Sprite):
  def __init__(self, position):
    super(Bomb, self).__init__()
    self.index = 0

    self.images = []
    for filename in os.listdir(PATH):
      if filename.endswith(".png"):
        self.images.append( pygame.image.load(PATH + filename))
        
    self.image = self.images[self.index]
    self.position = [position[0], position[1]]
    self.rect = pygame.Rect(position[0], position[1], SIZE_ITEM, SIZE_ITEM)
    
    self.explosion = False
    self.time_explosion = 8

  def update(self):
    if self.position[1] > GAME_HEIGHT:
      self.kill()
    elif self.explosion:
      self.time_explosion -= 1
      self.index += 1
      if self.index >= len(self.images) - 1:
          self.index = 0
      if self.time_explosion <= 0:
          self.kill()
      self.image = self.images[self.index]
      self.rect =pygame.Rect(self.position[0], self.position[1], SIZE_ITEM, SIZE_ITEM)
    else:
      self.position[1] += 2
      self.rect =pygame.Rect(self.position[0], self.position[1], SIZE_ITEM, SIZE_ITEM)

  def to_explosion(self):
    self.explosion = True

  def expire(self):
    return self.position[1] > GAME_WIDTH

  def collision(self, hammer):
    return self.rect.colliderect(hammer)
