import pygame
import colors
from const import *
from Text import *

pygame.init()

WIDTH = WIDTH_OF_PAUSE_GAME
HEIGHT = HEIGHT_OF_PAUSE_GAME

MARGIN = WIDTH / 6
BUTTON_WIDTH = 100
BUTTON_HEIGHT = 50

class PauseGame(pygame.Surface):
  def __init__(self):
    super(PauseGame, self).__init__((WIDTH, HEIGHT))
    self.fill(colors.DarkOrchid)
    pygame.draw.rect(self, colors.DarkOrchid, (0, 0, WIDTH, HEIGHT))
    y = HEIGHT / 2
    # PAUSE
    titleSurf, titleRect = Text.makeTextPauseGame('PAUSE', colors.Black)
    titleRect.center = (WIDTH / 2,  y)
    
    self.pause = pygame.Rect(WIDTH - MARGIN - BUTTON_WIDTH, y - BUTTON_HEIGHT / 2, BUTTON_WIDTH, BUTTON_HEIGHT)

    # pygame.draw.rect(self, colors.White, self.pause)
    self.blit(titleSurf, titleRect)
