import pygame
from const import *
import time

SIZE = 50

class Hammer(pygame.sprite.Sprite):
  def __init__(self):
    super(Hammer, self).__init__()
    self.index = 0
    self.images=(
      pygame.image.load("assets/images/hammer/1.png"),
      pygame.image.load("assets/images/hammer/2.png"),
      pygame.image.load("assets/images/hammer/3.png"),
      pygame.image.load("assets/images/hammer/4.png"),
      pygame.image.load("assets/images/hammer/5.png"),
      pygame.image.load("assets/images/hammer/6.png"))
    
    self.image=self.images[0]
    (a, b) = pygame.mouse.get_pos()
    self.rect = pygame.Rect(a, b, SIZE, SIZE)
    self.animation = 0

  def hit(self):
    #time.sleep(0.02)
    self.animation -=1
    self.index += 1

    if self.index >= len(self.images):
      self.index = 0
    self.image = self.images[self.index]
    (a, b) = pygame.mouse.get_pos()
    self.rect = pygame.Rect(a, b, SIZE, SIZE)

  def update(self, hit = False):
    if hit:
      self.animation = len(self.images)
      
    if self.animation != 0:
      self.hit()       
    
    (a, b) = pygame.mouse.get_pos()

    size_hammer = 120
    self.rect = pygame.Rect(a - size_hammer / 2, b - size_hammer / 2, SIZE, SIZE)
    
  def returnRec(self):
    return self.rect
