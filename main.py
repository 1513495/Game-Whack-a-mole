import sys, pygame, sched, os
from Hammer import *
import colors
from Sounds import *
import asyncio
from Text import *
from Background import *
from Mouse import *
from Player import *
from Heart import *
from Bomb import *
from functions import *
from PlayAgainBoarding import *
from const import *
from PauseGame import *
os.environ['SDL_VIDEO_WINDOW_POS'] = "%d, %d" % (200, 80)

pygame.init()

player = Player()
sounds = Sounds()

hammer_group = pygame.sprite.Group(Hammer())
mouse_group = pygame.sprite.Group()
heart_group = pygame.sprite.Group()
bomb_group = pygame.sprite.Group()

background = Background()

playAgainBoarding = PlayAgainBoarding()

pauseGame = PauseGame()

clock = pygame.time.Clock()

screen = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
pygame.display.set_caption("WHACK A MOLE - Top1Pubg")

def setting():
  pygame.mouse.set_visible(False)

def renderElectricBoard():
  Text.showText('Your Score:', colors.BurlyWood, screen, 50)
  Text.showText(str(player.score), colors.Red, screen, 100)
  Text.showText('Your Miss:', colors.BurlyWood, screen, 150)
  Text.showText(str(player.miss), colors.Red, screen, 200)
  Text.showText('Your Heart:', colors.BurlyWood, screen, 250)
  Text.showText(str(player.heart), colors.Red, screen, 300)

def renderPlayAgainBoarding():
  pygame.mouse.set_visible(True)
  while True:
    x = (WINDOW_WIDTH - WIDTH_OF_PLAY_AGAIN_BOARDING) / 2
    y = (WINDOW_HEIGHT - HEIGHT_OF_PLAY_AGAIN_BOARDIG) / 2
    screen.blit(playAgainBoarding, (x, y))
    for event in pygame.event.get():
      if event.type == pygame.QUIT:
        pygame.quit()
        sys.exit()
      if event.type == pygame.MOUSEBUTTONDOWN:
        (x, y) = pygame.mouse.get_pos()

        check = playAgainBoarding.click(x, y)
        if check is True:
          return True
        elif check is False:
          return False

    pygame.display.update()
    clock.tick(FPS)

def renderPauseGame(isPause):
  pygame.mouse.set_visible(False)
  while isPause:
    x = (WINDOW_WIDTH - WIDTH_OF_PAUSE_GAME) / 2
    y = (WINDOW_HEIGHT - HEIGHT_OF_PAUSE_GAME ) / 2
    screen.blit(pauseGame, (x, y))
    for event in pygame.event.get():
      if event.type == pygame.QUIT: 
        pygame.quit()
        sys.exit()
      if event.type == pygame.KEYDOWN:
        if event.key == pygame.K_p:
          return False
    pygame.display.update()
    clock.tick(FPS)

def main():
  setting()
  sounds.music()
  isPause = False
  idx = FRAMS_TO_RERENDER_NEW_OBJECT
  while not player.is_game_over():
    screen.blit(background, (0, 0))

    renderElectricBoard()
    if isPause is False : 
      for event in pygame.event.get():
        if event.type == pygame.QUIT:
          pygame.quit()
          sys.exit()
        if event.type == pygame.KEYDOWN:
          if(event.key == pygame.K_p):
            isPause = True
        if event.type == pygame.MOUSEBUTTONDOWN:
          sounds.hit()
          hammer_group.update(True)
        
          # check collition
          for cur_hammer in hammer_group:
            for cur_sprite in mouse_group:
              if cur_sprite.collision(cur_hammer.returnRec()):
                player.increase_score()
                sounds.mouse_die()
                cur_sprite.to_die()
            
            for cur_sprite in heart_group:
              if cur_sprite.collision(cur_hammer.returnRec()):
                cur_sprite.kill()
                sounds.take_heart()
                player.plusHeart()
            
            for cur_sprite in bomb_group:
              if cur_sprite.collision(cur_hammer.returnRec()):
                player.subHeart()
                sounds.explosive()
                cur_sprite.to_explosion()
          player.increase_total()
            
      #random mouse or heart
      if idx == FRAMS_TO_RERENDER_NEW_OBJECT :
        idx = 0
        listObj = createListItem(player.score)

        for obj in listObj:
          if type(obj) is Mouse:
            mouse_group.add(obj)
          elif type(obj) is Heart:
            heart_group.add(obj)
          elif type(obj) is Bomb:
            bomb_group.add(obj)
      else:
        idx += 1

      #check mouse attack 
      for cur_sprite in mouse_group:
        if cur_sprite.attack():
          player.subHeart()
          cur_sprite.kill()

      #update
      bomb_group.update()
      mouse_group.update()
      heart_group.update()
      hammer_group.update()

      bomb_group.draw(screen)
      mouse_group.draw(screen)
      heart_group.draw(screen)
      hammer_group.draw(screen)

      pygame.display.update()
      clock.tick(FPS)
    else:
      isPause = renderPauseGame(isPause)

if __name__ == '__main__':
  main()
  while renderPlayAgainBoarding():
    player = Player()

    hammer_group = pygame.sprite.Group(Hammer())
    mouse_group = pygame.sprite.Group()
    heart_group = pygame.sprite.Group()
    bomb_group = pygame.sprite.Group()

    main()