import os
from Heart import *
from Mouse import *
from Bomb import *
from const import *
import numpy
import random

def rename():
  folder = 'assets/sounds/hits'
  for filename in os.listdir(folder):
    infilename = os.path.join(folder,filename)
    if not os.path.isfile(infilename): continue
    oldbase = os.path.splitext(filename)
    newname = infilename.replace('.mp3', '.wav')
    output = os.rename(infilename, newname)

def createListItem(score):
  # if score is 200 then always appear 3 items in 3 lines
  probability2 = score / 100
  probability3 = score / 200

  if probability3 >= 1:
    probability3 = 1

  if probability2 + probability3 >= 1:
    probability2 = 1 - probability3

  probability1 = 1 - probability2 - probability3
  if probability1 <= 0:
    probability1 = 0

  numberOfItems = numpy.random.choice([1, 2, 3], p=[probability1, probability2, probability3])

  listObj = []

  # choose 1 or 2 or 3 number [0, LINES -1]
  listNumber = random.sample(range(0, LINES), numberOfItems);

  for i in listNumber:
    position = ((WIDTH_LINE - SIZE_ITEM) / 2 - BORDER_WIDTH + i * WIDTH_LINE, 0 )

    # random with probabilities:    80% mouse, 15% bomb, 5% heart
    item = numpy.random.choice([1, 2, 3], p=[0.8, 0.15, 0.05])
    if item == 1:
      listObj.append(Mouse(position))
    if item == 2:
      listObj.append(Bomb(position))
    if item == 3:
      listObj.append(Heart(position))

  return listObj
